﻿namespace DBMS_lab1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.parentDataView = new System.Windows.Forms.DataGridView();
            this.childDataView = new System.Windows.Forms.DataGridView();
            this.displayParentRecords = new System.Windows.Forms.Button();
            this.displayChildRecords = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.positionX = new System.Windows.Forms.TextBox();
            this.positionY = new System.Windows.Forms.TextBox();
            this.positionZ = new System.Windows.Forms.TextBox();
            this.properties = new System.Windows.Forms.GroupBox();
            this.addButton = new System.Windows.Forms.Button();
            this.update = new System.Windows.Forms.GroupBox();
            this.updateButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.parentDataView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.childDataView)).BeginInit();
            this.properties.SuspendLayout();
            this.update.SuspendLayout();
            this.SuspendLayout();
            // 
            // parentDataView
            // 
            this.parentDataView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.parentDataView.Location = new System.Drawing.Point(30, 35);
            this.parentDataView.Name = "parentDataView";
            this.parentDataView.RowHeadersWidth = 51;
            this.parentDataView.RowTemplate.Height = 24;
            this.parentDataView.Size = new System.Drawing.Size(425, 259);
            this.parentDataView.TabIndex = 0;
            this.parentDataView.Visible = false;
            this.parentDataView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.parentDataView_CellClick);
            // 
            // childDataView
            // 
            this.childDataView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.childDataView.Location = new System.Drawing.Point(639, 35);
            this.childDataView.Name = "childDataView";
            this.childDataView.RowHeadersWidth = 51;
            this.childDataView.RowTemplate.Height = 24;
            this.childDataView.Size = new System.Drawing.Size(455, 259);
            this.childDataView.TabIndex = 1;
            this.childDataView.Visible = false;
            this.childDataView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.childDataView_CellClick);
            // 
            // displayParentRecords
            // 
            this.displayParentRecords.Location = new System.Drawing.Point(180, 376);
            this.displayParentRecords.Name = "displayParentRecords";
            this.displayParentRecords.Size = new System.Drawing.Size(231, 101);
            this.displayParentRecords.TabIndex = 2;
            this.displayParentRecords.Text = "Display all parent records";
            this.displayParentRecords.UseVisualStyleBackColor = true;
            this.displayParentRecords.Click += new System.EventHandler(this.displayParentRecords_Click);
            // 
            // displayChildRecords
            // 
            this.displayChildRecords.Location = new System.Drawing.Point(180, 496);
            this.displayChildRecords.Name = "displayChildRecords";
            this.displayChildRecords.Size = new System.Drawing.Size(231, 74);
            this.displayChildRecords.TabIndex = 3;
            this.displayChildRecords.Text = "Display specific child records";
            this.displayChildRecords.UseVisualStyleBackColor = true;
            this.displayChildRecords.Click += new System.EventHandler(this.displayChildRecords_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(85, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Position X";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(85, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Position Z";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(85, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Position Y";
            // 
            // positionX
            // 
            this.positionX.Location = new System.Drawing.Point(177, 70);
            this.positionX.Name = "positionX";
            this.positionX.Size = new System.Drawing.Size(100, 22);
            this.positionX.TabIndex = 8;
            // 
            // positionY
            // 
            this.positionY.Location = new System.Drawing.Point(177, 100);
            this.positionY.Name = "positionY";
            this.positionY.Size = new System.Drawing.Size(100, 22);
            this.positionY.TabIndex = 9;
            // 
            // positionZ
            // 
            this.positionZ.Location = new System.Drawing.Point(177, 128);
            this.positionZ.Name = "positionZ";
            this.positionZ.Size = new System.Drawing.Size(100, 22);
            this.positionZ.TabIndex = 10;
            // 
            // properties
            // 
            this.properties.Controls.Add(this.addButton);
            this.properties.Controls.Add(this.label3);
            this.properties.Controls.Add(this.positionZ);
            this.properties.Controls.Add(this.label1);
            this.properties.Controls.Add(this.positionY);
            this.properties.Controls.Add(this.label2);
            this.properties.Controls.Add(this.positionX);
            this.properties.Location = new System.Drawing.Point(465, 361);
            this.properties.Name = "properties";
            this.properties.Size = new System.Drawing.Size(336, 200);
            this.properties.TabIndex = 11;
            this.properties.TabStop = false;
            this.properties.Text = "groupBox1";
            this.properties.Visible = false;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(214, 153);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(116, 41);
            this.addButton.TabIndex = 11;
            this.addButton.Text = "ADD";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // update
            // 
            this.update.Controls.Add(this.updateButton);
            this.update.Controls.Add(this.label4);
            this.update.Controls.Add(this.textBox1);
            this.update.Controls.Add(this.label5);
            this.update.Controls.Add(this.textBox2);
            this.update.Controls.Add(this.label6);
            this.update.Controls.Add(this.textBox3);
            this.update.Location = new System.Drawing.Point(807, 361);
            this.update.Name = "update";
            this.update.Size = new System.Drawing.Size(336, 200);
            this.update.TabIndex = 12;
            this.update.TabStop = false;
            this.update.Text = "groupBox1";
            this.update.Visible = false;
            // 
            // updateButton
            // 
            this.updateButton.Location = new System.Drawing.Point(214, 153);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(116, 41);
            this.updateButton.TabIndex = 11;
            this.updateButton.Text = "Update";
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(85, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Position Y";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(177, 128);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 22);
            this.textBox1.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(85, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Position X";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(177, 100);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 22);
            this.textBox2.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(85, 133);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Position Z";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(177, 70);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 22);
            this.textBox3.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1154, 582);
            this.Controls.Add(this.update);
            this.Controls.Add(this.properties);
            this.Controls.Add(this.displayChildRecords);
            this.Controls.Add(this.displayParentRecords);
            this.Controls.Add(this.childDataView);
            this.Controls.Add(this.parentDataView);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.parentDataView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.childDataView)).EndInit();
            this.properties.ResumeLayout(false);
            this.properties.PerformLayout();
            this.update.ResumeLayout(false);
            this.update.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView parentDataView;
        private System.Windows.Forms.DataGridView childDataView;
        private System.Windows.Forms.Button displayParentRecords;
        private System.Windows.Forms.Button displayChildRecords;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox positionX;
        private System.Windows.Forms.TextBox positionY;
        private System.Windows.Forms.TextBox positionZ;
        private System.Windows.Forms.GroupBox properties;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.GroupBox update;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox3;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMS_lab1
{
    public partial class Form1 : Form
    {
        private SqlConnection con = null;
        private String connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\Camelia\\Desktop\\DBMS\\DBMS_lab1\\Database1.mdf;Integrated Security=True";

        public Form1()
        {
            try
            {
                con = new SqlConnection(connectionString);
                con.Open();
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.Message);
            }

            InitializeComponent();
        }

        private void displayParentRecords_Click(object sender, EventArgs e)
        {
            parentDataView.Visible = true;
            String querry = "SELECT * FROM Can";
            SqlCommand cmd = new SqlCommand(querry, con);

            cmd.CommandType = CommandType.Text;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds, "Can");

            parentDataView.DataSource = ds.Tables["Can"];
            DataGridViewButtonColumn addButtonColumn = new DataGridViewButtonColumn();
            addButtonColumn.Name = "addColumn";

            addButtonColumn.HeaderText = "add new child";
            parentDataView.Columns.Insert(ds.Tables["Can"].Columns.Count, addButtonColumn);
            foreach (DataGridViewRow row in parentDataView.Rows)
            {
                row.Cells["addColumn"].Value = "add new child";
            }

        }

        private void displayChildRecords_Click(object sender, EventArgs e)
        {
            childDataView.Refresh();
            // childDataView.Rows.Clear();
            childDataView.Columns.Clear();
            childDataView.DataSource = null;
            childDataView.Visible = true;
            if (parentDataView.SelectedRows.Count == 1)
            {
                int selectedRow = parentDataView.CurrentCell.RowIndex;
                DataGridViewRow row = parentDataView.Rows[selectedRow];
                string selectedId = Convert.ToString(row.Cells["Id"].Value);
                String querry = "SELECT * FROM Car WHERE CarId = " + selectedId;
                SqlCommand cmd = new SqlCommand(querry, con);

                cmd.CommandType = CommandType.Text;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds, "Car");

                childDataView.DataSource = ds.Tables["Car"];

                DataGridViewButtonColumn removeButtonColumn = new DataGridViewButtonColumn();
                removeButtonColumn.Name = "removeColumn";

                removeButtonColumn.HeaderText = "remove child";
                childDataView.Columns.Insert(ds.Tables["Car"].Columns.Count, removeButtonColumn);

                DataGridViewButtonColumn updateButtonColumn = new DataGridViewButtonColumn();
                updateButtonColumn.Name = "updateColumn";

                updateButtonColumn.HeaderText = "update child";
                childDataView.Columns.Insert(ds.Tables["Car"].Columns.Count+1, updateButtonColumn);
                foreach (DataGridViewRow r in childDataView.Rows)
                {
                    r.Cells["updateColumn"].Value = "update child";
                    r.Cells["removeColumn"].Value = "remove child";
                }
            }
        }

        private void parentDataView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                properties.Visible = true;
                positionX.Text = "";
                positionY.Text = "";
                positionZ.Text = "";
                properties.Text = "Add a new child for the Can id = " + parentDataView.Rows[e.RowIndex].Cells[0].Value.ToString();
            }

        }

        private void addButton_Click(object sender, EventArgs e)
        {
            string id = properties.Text.Substring(33);
            string distance = Math.Sqrt(Int32.Parse(positionX.Text) * Int32.Parse(positionX.Text) + Int32.Parse(positionY.Text) * Int32.Parse(positionY.Text) + Int32.Parse(positionZ.Text) * Int32.Parse(positionZ.Text)).ToString();
            String querry = "INSERT INTO Car(CarId, PositionX, PositionY, PositionZ, Distance) VALUES (" + id + ", " + Int32.Parse(positionX.Text) + "," + Int32.Parse(positionY.Text) + ", " + Int32.Parse(positionZ.Text) + "," + distance + ")";
            SqlCommand com = new SqlCommand(querry, con);
            com.ExecuteNonQuery();
            properties.Visible = false;
        }

        private void childDataView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
            {
                MessageBox.Show(e.RowIndex.ToString());
                string id = childDataView.Rows[e.RowIndex].Cells[0].Value.ToString();
                MessageBox.Show(id.ToString());
                string querry = "DELETE FROM Car WHERE CId = " + id;

                SqlCommand com = new SqlCommand(querry, con);
                com.ExecuteNonQuery();
                childDataView.Update();
                childDataView.Refresh();
            }
            else if (e.ColumnIndex == 7)
            {
                update.Visible = true;
                update.Text = "Update child with id = " + childDataView.Rows[e.RowIndex].Cells[0].Value.ToString();

            }
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            string id = update.Text.Substring(23);
            string distance = Math.Sqrt(Int32.Parse(textBox3.Text) * Int32.Parse(textBox3.Text) + Int32.Parse(textBox2.Text) * Int32.Parse(textBox2.Text) + Int32.Parse(textBox1.Text) * Int32.Parse(textBox1.Text)).ToString();
            String querry = "UPDATE Car SET PositionX = " + Int32.Parse(textBox3.Text) + ", PositionY = " + Int32.Parse(textBox2.Text) + ", PositionZ = " + Int32.Parse(textBox1.Text) + ", Distance = " + distance + "WHERE CId = " + id;
            SqlCommand com = new SqlCommand(querry, con);
            com.ExecuteNonQuery();
            childDataView.Update();
            childDataView.Refresh();
            update.Visible = false;
        }
    }
    }

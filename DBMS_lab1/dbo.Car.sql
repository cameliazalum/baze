﻿CREATE TABLE [dbo].[Car] (
	[Id] INT NOT NULL IDENTITY, 
    [CarId]     INT  NULL,
    [PositionX] INT  NULL,
    [PositionY] INT  NULL,
    [PositionZ] INT  NULL,
    [Distance] NCHAR(100) NULL, 
    
    CONSTRAINT [FK_Car_Can] FOREIGN KEY ([CarId]) REFERENCES [dbo].[Can] ([DetectedCar]), 
    CONSTRAINT [PK_Car] PRIMARY KEY ([Id])
);


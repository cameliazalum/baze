﻿CREATE TABLE [dbo].[Car]
(
	[CarId] INT NOT NULL PRIMARY KEY REFERENCES (Can.CarId), 
    [PositionX] INT NULL, 
    [PositionY] INT NULL, 
    [PositionZ] INT NULL, 
    [Distance] INT NULL
)
